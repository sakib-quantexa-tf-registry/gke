resource "google_container_cluster" "gkedeploy_zonal_cluster" {
  count    = var.deploy_gke_cluster ? 1 : 0
  project  = var.gcp_project_id
  name     = "gkedeploy-cluster"
  location = var.gcp_default_zone

  # create the smallest possible default node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  networking_mode          = "VPC_NATIVE"
  network                  = var.network_name
  subnetwork               = var.subnetwork_name

  private_cluster_config {
    enable_private_nodes   = true
    master_ipv4_cidr_block = "10.13.0.0/28"
  }

  ip_allocation_policy {
    cluster_ipv4_cidr_block  = "10.11.0.0/21"
    services_ipv4_cidr_block = "10.12.0.0/21"
  }
}

resource "google_container_node_pool" "gkedeploy_cheappool" {
  count      = var.deploy_gke_cluster ? 1 : 0
  name       = "gkedeploy-cheappool"
  project    = var.gcp_project_id
  location   = var.gcp_default_zone
  cluster    = google_container_cluster.gkedeploy_zonal_cluster[0].id
  node_count = 1
  node_config {
    preemptible  = true
    machine_type = "e2-standard-2"
    disk_size_gb = 30

    # The service account to be used by the Node VMs, grant permission through IAM
    service_account = var.service_account_email
  }
}
