variable "gcp_project_id" {
  type        = string
  description = "The GCP project id"
}

variable "gcp_default_region" {
  type        = string
  description = "The GCP region where create the resources"
  default     = "europe-west2"
}

variable "gcp_default_zone" {
  type        = string
  description = "The GCP zone where create the resources"
  default     = "europe-west2-a"
}

variable "deploy_gke_cluster" {
  type        = bool
  default     = true
  description = "set to `false` to remove the GKE"
}

variable "network_name" {
  type        = string
  description = "The name of the network"
}

variable "subnetwork_name" {
  type        = string
  description = "The name of the subnetwork"
}

variable "service_account_email" {
  type        = string
  description = "The email address of the service account"
}
